package losdirectores;

import sqlconnector.sqlconnector;

public class losdirectores {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		// Creo el objeto conexion
		sqlconnector conexion = new sqlconnector();
		
		// Me conecto a la base de datos
		conexion.startConnection();
		
		// Creo la base de datos
		conexion.createDB("losdirectores");
		
		// Creo las tablas
		conexion.createTableDespachos("losdirectores", "despachos");
		conexion.createTableDirectores("losdirectores", "directores");
		
		// Inserto datos
		conexion.insertDataDespachos("losdirectores", "despachos");
		conexion.insertDataDirectores("losdirectores", "directores");
		
		// Cierro la conexion
		conexion.closeConnection();
	}

}
