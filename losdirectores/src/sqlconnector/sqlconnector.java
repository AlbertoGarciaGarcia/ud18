package sqlconnector;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class sqlconnector {

	// Creo la constante conexion
	public static Connection conexion;
	
	// Creo el metodo que conectar con la base de datos
	public void startConnection() {
		
		try {
			
			Class.forName("com.mysql.cj.jdbc.Driver");
			
			conexion = DriverManager.getConnection("jdbc:mysql://192.168.1.XXX:3306?useTimezone=true&serverTimezone=UTC", "USER", "PASS");
			
			System.out.println("Server Connected");
			
		} catch (SQLException | ClassNotFoundException ex) {
			System.out.println("No se ha podido conectar con la base de datos");
			System.out.println(ex);
		}
	}
	
	// Mtodo que desconectar
	public void closeConnection() {
		
		try {
			
			conexion.close();
			System.out.println("Server Disconnected");
			
		} catch (SQLException ex) {
			System.out.println(ex.getMessage());
			System.out.println("Error cerrando conexion");
		}
	}
	
	// Mtodo que crear la base de datos
	public void createDB(String name) {
		
		try {
			String Query = "CREATE DATABASE " + name;
			Statement st = conexion.createStatement();
			st.executeUpdate(Query);
			System.out.println("DB creada con exito.");
			
			System.out.println("Se ha creado la DB " + name + " de forma exitosa.");
			
		} catch (SQLException ex) {
			System.out.println(ex.getMessage());
			System.out.println("Error creando la DB.");
		}
	}
	
	// Mtodos que crearn las tablas
	public void createTableDespachos(String db, String name) {
		
		try {
			String Querydb = "USE " + db + ";";
			
			Statement stdb = conexion.createStatement();
			
			stdb.executeUpdate(Querydb);
			
			String Query = "CREATE TABLE " + name + "(`numero` int NOT NULL,\r\n" + 
					"  `capacidad` int DEFAULT NULL,\r\n" + 
					"  PRIMARY KEY (`numero`))";
				
				Statement st = conexion.createStatement();
				
				st.executeUpdate(Query);
				System.out.println("Tabla creada con exito.");
			
		} catch (SQLException ex) {
			System.out.println(ex.getMessage());
			System.out.println("Error creando tabla.");
		}
	}
	
	public void createTableDirectores(String db, String name) {
		
		try {
			
			String Querydb = "USE " + db + ";";
			
			Statement stdb = conexion.createStatement();
			
			stdb.executeUpdate(Querydb);
			
			String Query = "CREATE TABLE " + name + "(`DNI` varchar(8) NOT NULL,\r\n" + 
					"  `NomApels` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,\r\n" + 
					"  `despacho` int NOT NULL,\r\n" + 
					"  `DNIJefe` varchar(8) NOT NULL,\r\n" + 
					"  PRIMARY KEY (`DNI`))";
			
			Statement st = conexion.createStatement();
			
			st.executeLargeUpdate(Query);
			System.out.println("Tabla creada con exito");
			
		} catch (SQLException ex) {
			System.out.println(ex.getMessage());
			System.out.println("Error creando tabla.");
		}
		
	}
	
	// Mtodos que insertarn los datos
	public void insertDataDespachos(String db, String tableName) {
		
		try {
			String Querydb = "USE " + db + ";";
			
			Statement stdb = conexion.createStatement();
			
			stdb.executeLargeUpdate(Querydb);
			
			String Query = "INSERT INTO " + tableName + " (numero, capacidad) VALUE"
																			+ "(1, 20),"
																			+ "(2, 10),"
																			+ "(3, 30),"
																			+ "(4, 15),"
																			+ "(5, 5);";
			
			Statement st = conexion.createStatement();
			st.executeUpdate(Query);
			
			System.out.println("Datos almacenados correctamente");
			
		} catch (SQLException ex) {
			System.out.println(ex.getMessage());
			System.out.println("Error en el almacenamiento");
		}
	}
	
	public void insertDataDirectores(String db, String tableName) {
		
		try {
			String Querydb = "USE " + db + ";";
			
			Statement stdb = conexion.createStatement();
			
			stdb.executeLargeUpdate(Querydb);
			
			String Query = "INSERT INTO " + tableName + " (DNI, NomApels, despacho, DNIJefe) VALUE"
																							+ "('1234567H', 'Aitor Alarcn', 1, '1234567J'),"
																							+ "('1234567J', 'Jess Cristo', 5, '1234567H'),"
																							+ "('1234567M', 'Alberto Garca', 2, '1234567H'),"
																							+ "('1234567N', 'Oscar Trillas', 3, '1234567R'),"
																							+ "('1234567R', 'Aleix Jimenez', 4, '1234567N');";
			
			Statement st = conexion.createStatement();
			st.executeUpdate(Query);
			
			System.out.println("Datos almacenados correctamente");
			
		} catch (SQLException ex) {
			System.out.println(ex.getMessage());
			System.out.println("Error en el almacenamiento");
		}
		
	}
}
