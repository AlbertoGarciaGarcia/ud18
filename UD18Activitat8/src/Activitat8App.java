import dto.metodos;

public class Activitat8App {

	public static void main(String[] args) {
		metodos.conexion();
		metodos.crearBaseDatos("LosGrandesAlmacenes");
		metodos.crearTabla("LosGrandesAlmacenes", "cajeros", "(codigo int PRIMARY KEY, nomApels nvarchar(255))");
		metodos.crearTabla("LosGrandesAlmacenes", "productos", "(codigo int PRIMARY KEY, nombre nvarchar(100), precio int)");
		metodos.crearTabla("LosGrandesAlmacenes", "maquinas_registradoras", "(codigo int PRIMARY KEY, piso int)");
		metodos.crearTabla("LosGrandesAlmacenes", "venta", "(cajero int, maquina int, producto int, PRIMARY KEY (cajero, maquina, producto), "
															+ "FOREIGN KEY(cajero) REFERENCES cajeros(codigo),"
															+ "FOREIGN KEY(maquina) REFERENCES maquinas_registradoras(codigo),"
															+ "FOREIGN KEY(producto) REFERENCES productos(codigo))");
		metodos.introducirInformacion();
		metodos.cerrarConexion();

	}

}
