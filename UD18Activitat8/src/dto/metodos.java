package dto;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class metodos {
	public static Connection conexion;
	//Metodo de conexion
	public static void conexion() {
		try {
	        Class.forName("com.mysql.cj.jdbc.Driver");
	        conexion = DriverManager.getConnection("jdbc:mysql://192.168.1.44:3306?useTimezone=true&serverTimezone=UTC","remot","Diplodocus1.");
	        System.out.println("Servidor conectado");
	    }catch(SQLException | ClassNotFoundException ex) {
	        System.out.println("No se ha conectado");
	        System.out.println(ex);
	    }
		
	}
	
	//Metodo para cerrar la conexion
	public static void cerrarConexion() {
		try {
			conexion.close();
			System.out.print("Server Disconnected");
		} catch (SQLException ex) {
			System.out.println(ex.getMessage());
			System.out.print("Error cerrando conexion");
		}
	}
	
	//Metodo para crear una tabla
	public static void crearTabla(String db, String nombre, String insertar) {
		try {
			String querydb = "USE  " + db + ";";
			Statement stdb = conexion.createStatement();
			stdb.executeLargeUpdate(querydb);
			String query = "CREATE TABLE " + nombre + "" + insertar;
			Statement st = conexion.createStatement();
			st.executeLargeUpdate(query);
			System.out.println("Tabla Creada");
			
		}catch (SQLException ex){
			System.out.println(ex.getMessage());
			System.out.println("Error crando tabla.");
			
		}
	}
	
	//Metodo para crear una base de datos
	public static void crearBaseDatos(String nombre) {
		try {
			String Query="CREATE DATABASE "+ nombre;
			Statement st= conexion.createStatement();
			st.executeUpdate(Query);
			System.out.println("DB creada con exito!");	
			System.out.println("Base de datos creada");
		}catch(SQLException ex) {
			System.out.println(ex.getMessage());
			System.out.println("Error creando la DB.");
		}	
	}
	
	//Metodo para insertar valores dentro de la tabla departamento
	public static void insertarValoresCajeros(String db, String nombreTabla, int codigo, String nomApels) {
		try {
			String Querydb = "USE " + db + ";";
			Statement stdb = conexion.createStatement();
			stdb.executeUpdate(Querydb);
			String Query = "INSERT INTO " + nombreTabla + "(codigo, nomApels) VALUE("
							+ "\"" + codigo + "\","
							+ "\"" + nomApels + "\");";
			Statement st = conexion.createStatement();
			st.executeUpdate(Query);
			
			System.out.println("Datos guardados");
		} catch (SQLException ex) {
			System.out.println("No se ha podido guardar los datos");
			System.out.println(ex.getMessage());
		}
	}
	
	//Metodo para insertar valores dentro de la tabla empleados
	public static void insertarValoresMaquinas(String db, String nombreTabla, int codigo, int piso) {
		try {
			String Querydb = "USE " + db + ";";
			Statement stdb = conexion.createStatement();
			stdb.executeUpdate(Querydb);
			String Query = "INSERT INTO " + nombreTabla + "(codigo, piso) VALUE("
							+ "\"" + codigo + "\","
							+ "\"" + piso + "\");";
			Statement st = conexion.createStatement();
			st.executeUpdate(Query);
			
			System.out.println("Datos guardados");
		} catch (SQLException ex) {
			System.out.println("No se ha podido guardar los datos");
			System.out.println(ex.getMessage());
		}
	}
	
	public static void insertarValoresProductos(String db, String nombreTabla, int codigo, String nombre, int precio) {
		try {
			String Querydb = "USE " + db + ";";
			Statement stdb = conexion.createStatement();
			stdb.executeUpdate(Querydb);
			String Query = "INSERT INTO " + nombreTabla + "(codigo, nombre, precio) VALUE("
							+ "\"" + codigo + "\","
							+ "\"" + nombre + "\","
							+ "\"" + precio + "\");";
			Statement st = conexion.createStatement();
			st.executeUpdate(Query);
			
			System.out.println("Datos guardados");
		} catch (SQLException ex) {
			System.out.println("No se ha podido guardar los datos");
			System.out.println(ex.getMessage());
		}
	}
	
	public static void insertarValoresVenta(String db, String nombreTabla, int cajero, int maquina, int producto) {
		try {
			String Querydb = "USE " + db + ";";
			Statement stdb = conexion.createStatement();
			stdb.executeUpdate(Querydb);
			String Query = "INSERT INTO " + nombreTabla + "(cajero, maquina, producto) VALUE("
							+ "\"" + cajero + "\","
							+ "\"" + maquina + "\","
							+ "\"" + producto + "\");";
			Statement st = conexion.createStatement();
			st.executeUpdate(Query);
			
			System.out.println("Datos guardados");
		} catch (SQLException ex) {
			System.out.println("No se ha podido guardar los datos");
			System.out.println(ex.getMessage());
		}
	}
	
	//Metodo que inserta todos los valores que quiero en las dos tablas
	public static void introducirInformacion() {
		//Inserto valores en la primera tabla
		insertarValoresCajeros("LosGrandesAlmacenes", "cajeros", 1, "Oscar");
		insertarValoresCajeros("LosGrandesAlmacenes", "cajeros", 2, "Sergi");
		insertarValoresCajeros("LosGrandesAlmacenes", "cajeros", 3, "Alberto");
		insertarValoresCajeros("LosGrandesAlmacenes", "cajeros", 4, "Aitor");
		insertarValoresCajeros("LosGrandesAlmacenes", "cajeros", 5, "Dani");
		
		//Inserto valores en la segunda tabla
		insertarValoresMaquinas("LosGrandesAlmacenes", "maquinas_registradoras", 1, 1);
		insertarValoresMaquinas("LosGrandesAlmacenes", "maquinas_registradoras", 2, 2);
		insertarValoresMaquinas("LosGrandesAlmacenes", "maquinas_registradoras", 3, 3);
		insertarValoresMaquinas("LosGrandesAlmacenes", "maquinas_registradoras", 4, 4);
		insertarValoresMaquinas("LosGrandesAlmacenes", "maquinas_registradoras", 5, 5);
		
		//Inserto valores en la tercera tabla
		insertarValoresProductos("LosGrandesAlmacenes", "productos", 1, "Pringels", 1);
		insertarValoresProductos("LosGrandesAlmacenes", "productos", 2, "Donuts", 3);
		insertarValoresProductos("LosGrandesAlmacenes", "productos", 3, "Patatas", 5);
		insertarValoresProductos("LosGrandesAlmacenes", "productos", 4, "Agua", 2);
		insertarValoresProductos("LosGrandesAlmacenes", "productos", 5, "Monster", 4);
		
		//Inserto valores en la cuarta tabla
		insertarValoresVenta("LosGrandesAlmacenes", "venta", 1, 1, 1);
		insertarValoresVenta("LosGrandesAlmacenes", "venta", 2, 2, 2);
		insertarValoresVenta("LosGrandesAlmacenes", "venta", 3, 3, 3);
		insertarValoresVenta("LosGrandesAlmacenes", "venta", 4, 4, 4);
		insertarValoresVenta("LosGrandesAlmacenes", "venta", 5, 5, 5);
	}
}
