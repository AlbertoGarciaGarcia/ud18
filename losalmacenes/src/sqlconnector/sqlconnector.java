package sqlconnector;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class sqlconnector {

	// Creo la constante conexion
	public static Connection conexion;
	
	// Creo el metodo que conectar con la base de datos
	public void startConnection() {
		
		try {
			
			Class.forName("com.mysql.cj.jdbc.Driver");
			
			conexion = DriverManager.getConnection("jdbc:mysql://192.168.1.XXX:3306?useTimezone=true&serverTimezone=UTC", "USER", "PASS");
			
			System.out.println("Server Connected");
			
		} catch (SQLException | ClassNotFoundException ex) {
			System.out.println("No se ha podido conectar con la base de datos");
			System.out.println(ex);
		}
	}
	
	// Mtodo que desconectar
	public void closeConnection() {
		
		try {
			
			conexion.close();
			System.out.println("Server Disconnected");
			
		} catch (SQLException ex) {
			System.out.println(ex.getMessage());
			System.out.println("Error cerrando conexion");
		}
	}
	
	// Mtodo que crear la base de datos
	public void createDB(String name) {
		
		try {
			String Query = "CREATE DATABASE " + name;
			Statement st = conexion.createStatement();
			st.executeUpdate(Query);
			System.out.println("DB creada con exito.");
			
			System.out.println("Se ha creado la DB " + name + " de forma exitosa.");
			
		} catch (SQLException ex) {
			System.out.println(ex.getMessage());
			System.out.println("Error creando la DB.");
		}
	}
	
	// Mtodos que crearn las tablas
	public void createTableAlmacenes(String db, String name) {
		
		try {
			String Querydb = "USE " + db + ";";
			
			Statement stdb = conexion.createStatement();
			
			stdb.executeUpdate(Querydb);
			
			String Query = "CREATE TABLE " + name + "(`Codigo` int NOT NULL AUTO_INCREMENT,\r\n" + 
				"  `Lugar` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,\r\n" + 
				"  `Capacidad` int DEFAULT NULL,\r\n" + 
				"  PRIMARY KEY (`Codigo`))";
				
				Statement st = conexion.createStatement();
				
				st.executeUpdate(Query);
				System.out.println("Tabla creada con exito.");
			
		} catch (SQLException ex) {
			System.out.println(ex.getMessage());
			System.out.println("Error creando tabla.");
		}
	}
	
	public void createTableCajas(String db, String name) {
		
		try {
			
			String Querydb = "USE " + db + ";";
			
			Statement stdb = conexion.createStatement();
			
			stdb.executeUpdate(Querydb);
			
			String Query = "CREATE TABLE " + name + "(`NumReferencia` char(5) NOT NULL,\r\n" + 
					"  `Contenido` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,\r\n" + 
					"  `Valor` int DEFAULT NULL,\r\n" + 
					"  `Almacen` int NOT NULL,\r\n" + 
					"  PRIMARY KEY (`NumReferencia`))";
			
			Statement st = conexion.createStatement();
			
			st.executeLargeUpdate(Query);
			System.out.println("Tabla creada con exito");
			
		} catch (SQLException ex) {
			System.out.println(ex.getMessage());
			System.out.println("Error creando tabla.");
		}
		
	}
	
	// Mtodos que insertarn los datos
	public void insertDataAlmacenes(String db, String tableName, String lugar, int capacidad) {
		
		try {
			String Querydb = "USE " + db + ";";
			
			Statement stdb = conexion.createStatement();
			
			stdb.executeLargeUpdate(Querydb);
			
			String Query = "INSERT INTO " + tableName + " (Lugar, Capacidad) VALUE(" + "'" + lugar + "'" + ", " + "'" + capacidad + "'" + ");";
			
			Statement st = conexion.createStatement();
			st.executeUpdate(Query);
			
			System.out.println("Datos almacenados correctamente");
			
		} catch (SQLException ex) {
			System.out.println(ex.getMessage());
			System.out.println("Error en el almacenamiento");
		}
	}
	
	public void insertDataCajas(String db, String tableName, String numReferencia, String contenido, int valor, int almacen) {
		
		try {
			String Querydb = "USE " + db + ";";
			
			Statement stdb = conexion.createStatement();
			
			stdb.executeLargeUpdate(Querydb);
			
			String Query = "INSERT INTO " + tableName + " (NumReferencia, Contenido, Valor, Almacen) VALUE(" + "'" + numReferencia + "'" + ", " + "'" + contenido + "'" + ", " + 
			"'" + valor + "'" + ", " + "'" + almacen + "'" + ");";
			
			Statement st = conexion.createStatement();
			st.executeUpdate(Query);
			
			System.out.println("Datos almacenados correctamente");
			
		} catch (SQLException ex) {
			System.out.println(ex.getMessage());
			System.out.println("Error en el almacenamiento");
		}
		
	}
}
