package losalmacenes;

import sqlconnector.sqlconnector;

public class losalmacenes {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
	
		// Creo el objeto conexion
		sqlconnector conexion = new sqlconnector();
		
		// Me conecto a la base de datos
		conexion.startConnection();
		
		// Creo la base de datos
		conexion.createDB("losalmacenes");
		
		// Creo las tablas
		conexion.createTableAlmacenes("losalmacenes", "almacenes");
		conexion.createTableCajas("losalmacenes", "cajas");
		
		// Inserto datos a almacenes
		conexion.insertDataAlmacenes("losalmacenes", "almacenes", "Barcelona", 5);
		conexion.insertDataAlmacenes("losalmacenes", "almacenes", "Tarragona", 4);
		conexion.insertDataAlmacenes("losalmacenes", "almacenes", "Lleida", 3);
		conexion.insertDataAlmacenes("losalmacenes", "almacenes", "Girona", 2);
		conexion.insertDataAlmacenes("losalmacenes", "almacenes", "Reus", 1);
		
		// Inserto datos a cajas
		conexion.insertDataCajas("losalmacenes", "cajas", "JGRSH", "Papeles", 150, 5);
		conexion.insertDataCajas("losalmacenes", "cajas", "JGR5S", "Rocas", 300, 4);
		conexion.insertDataCajas("losalmacenes", "cajas", "AAD00", "Tijeras", 400, 3);
		conexion.insertDataCajas("losalmacenes", "cajas", "VHV27", "Papeles", 175, 2);
		conexion.insertDataCajas("losalmacenes", "cajas", "LAJM4", "Rocas", 600, 1);
		
		// Cierro la conexion
		conexion.closeConnection();
	}

}
