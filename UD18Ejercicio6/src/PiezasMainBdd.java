import dto.metodos;
public class PiezasMainBdd {

	public static void main(String[] args) {
		metodos.conectar();
		metodos.crearBaseDatos();
		metodos.crearTablaPiezas();
		metodos.crearTablaProveedores();
		metodos.crearTablaSuministra();
		metodos.insertarTablaPiezas();
		metodos.insertarTablaProveedores();
		metodos.insertarTablaSuministra();
		metodos.desconectar();


	}

}
