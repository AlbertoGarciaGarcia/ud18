package dto;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
public class metodos {
	public static Connection conexion; // Establecemos la constante conexion
	{
	
		
		
	
	
}
	public static void conectar() {
		try { 
			Class.forName("com.mysql.cj.jdbc.Driver");
			 conexion = DriverManager.getConnection("jdbc:mysql://192.168.1.14:3306?useTimezone=true&serverTimezone=UTC","alberto","diplodocus1");
			System.out.println("Servidor conectado"); // Hacemos la conexion
		}catch(SQLException | ClassNotFoundException ex) {
			System.out.println("No se ha podido conectar con mi base de datos");
			System.out.println(ex);
		}
	}
	public static void desconectar() {
		try {
			
			conexion.close(); // Hacemos la desconexion
			System.out.print("Desconectado con exito");
			
		} catch (SQLException ex) {
			System.out.println(ex.getMessage());
			System.out.print("Ha habido un error");
		
		}
	}
	public static void crearBaseDatos() {
		String nombre = "TiendaPiezas"; // Creamos la base de datos
		try {
			String Query="CREATE DATABASE "+ nombre;
			Statement st= conexion.createStatement();
			st.executeUpdate(Query);
			System.out.println("DB creada con exito!");
			
			System.out.println("Se ha creado la base de datos");
		}catch(SQLException ex) {
			System.out.println(ex.getMessage());
			System.out.println("Error creando la base de datos.");
		}
	}
	
	
	public static void crearTablaPiezas() {
		String nombre = "TiendaPiezas"; // Creamos la tabla piezas
		String nombreTabla = "Piezas";
		try {
			String Querydb = "USE "+nombre+";";
			Statement stdb= conexion.createStatement();
			stdb.executeUpdate(Querydb);
			
			String Query = "CREATE TABLE "+nombreTabla+""
					+ "( `Codigo` int(11) NOT NULL AUTO_INCREMENT," + 
					"  `Nombre` varchar(100) CHARACTER SET utf8 DEFAULT NULL," + 
					"  PRIMARY KEY (`Codigo`))";
			Statement st= conexion.createStatement();
			st.executeUpdate(Query);
			System.out.println("Tabla creada");
			
		}catch (SQLException ex){
			System.out.println(ex.getMessage());
			System.out.println("Error creando tabla.");
			
		}
		
	}
	
	public static void crearTablaSuministra() {
		String nombre = "TiendaPiezas"; // Creamos la tabla suministra
		String nombreTabla = "Suministra";
		try {
			String Querydb = "USE "+nombre+";";
			Statement stdb= conexion.createStatement();
			stdb.executeUpdate(Querydb);
			
			String Query = "CREATE TABLE "+nombreTabla+""
					+ "  ( `CodigoPieza` int(11) NOT NULL," + 
					"  `IdProveedor` char(4) NOT NULL," + 
					"  `Precio` int(11) DEFAULT NULL," + 
					"  PRIMARY KEY (`CodigoPieza`,`IdProveedor`)," + 
					"  KEY `IdProveedor` (`IdProveedor`)," + 
					"   FOREIGN KEY (`CodigoPieza`) REFERENCES `Piezas`(`Codigo`)"
					+ " ON DELETE CASCADE ON UPDATE CASCADE,"
					+ " FOREIGN KEY (`IdProveedor`)"
					+ " REFERENCES `Proveedores`(`Id`) ON DELETE CASCADE ON UPDATE CASCADE);";
			Statement st= conexion.createStatement();
			st.executeUpdate(Query);
			System.out.println("Tabla creada");
			
		}catch (SQLException ex){
			System.out.println(ex.getMessage());
			System.out.println("Error creando tabla.");
			
		}
	}
	
	public static void crearTablaProveedores() {
		String nombre = "TiendaPiezas"; // Creamos la tabla provedoores
		String nombreTabla = "Proveedores";
		try {
			String Querydb = "USE "+nombre+";";
			Statement stdb= conexion.createStatement();
			stdb.executeUpdate(Querydb);
			
			String Query = "CREATE TABLE "+nombreTabla+""
					+ " (`Id` char(4) NOT NULL,\r\n" + 
					"  `Nombre` varchar(100) DEFAULT NULL,\r\n" + 
					"  PRIMARY KEY (`Id`));";
			Statement st= conexion.createStatement();
			st.executeUpdate(Query);
			System.out.println("Tabla creada");
			
		}catch (SQLException ex){
			System.out.println(ex.getMessage());
			System.out.println("Error creando tabla.");
			
		}
	}
	
	
	
	



public static void insertarTablaPiezas() {
	String nombre = "TiendaPiezas"; // Insertamos los datos en la tabla fabricante
	String nombreTabla = "Piezas";
	try {
		String Querydb = "USE "+nombre+";";
		Statement stdb= conexion.createStatement();
		stdb.executeUpdate(Querydb);
					
		String Query = "INSERT INTO " + nombreTabla + " (Codigo,Nombre) VALUE"
				+ " (1,'Bujia'),"
				+ "(2,'Marcha'),"
				+ "(3,'Bobina'),"
				+ "(4,'Cadena'),"
				+ "(5,'Transmisor');";
		Statement st = conexion.createStatement();
		st.executeUpdate(Query);
		
		System.out.println("Datos almacenados correctamente");;
		
	} catch (SQLException ex ) {
		System.out.println(ex.getMessage());
	System.out.println("Error en la introduccion de datos");
	}
}
public static void insertarTablaProveedores() {
	String nombre = "TiendaPiezas"; // Insertamos los datos en la tabla articulos
	String nombreTabla = "Proveedores";
	try {
		String Querydb = "USE "+nombre+";";
		Statement stdb= conexion.createStatement();
		stdb.executeUpdate(Querydb);
					
		String Query = "INSERT INTO " + nombreTabla + " (Id,Nombre) VALUE"
				+ "(1,'Suzuki'),"
				+ "(2,'Mitsubishi'),"
				+ "(3,'Toyota'),"
				+ "(4,'Honda'),"
				+ "(5,'Subaru');";
		Statement st = conexion.createStatement();
		st.executeUpdate(Query);
		
		System.out.println("Datos almacenados correctamente");;
		
	} catch (SQLException ex ) {
		System.out.println(ex.getMessage());
	System.out.println("Error en la introduccion de datos");
	}
}
public static void insertarTablaSuministra() {
	String nombre = "TiendaPiezas"; // Insertamos los datos en la tabla articulos
	String nombreTabla = "Suministra";
	try {
		String Querydb = "USE "+nombre+";";
		Statement stdb= conexion.createStatement();
		stdb.executeUpdate(Querydb);
					
		String Query = "INSERT INTO " + nombreTabla + " (CodigoPieza,IdProveedor,Precio) VALUE"
				+ "(1,2,340),"
				+ "(2,3,130),"
				+ "(3,5,140),"
				+ "(4,1,270),"
				+ "(5,4,700);";
		Statement st = conexion.createStatement();
		st.executeUpdate(Query);
		
		System.out.println("Datos almacenados correctamente");;
		
	} catch (SQLException ex ) {
		System.out.println(ex.getMessage());
	System.out.println("Error en la introduccion de datos");
	}
}



}
