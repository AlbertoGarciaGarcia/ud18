import dto.metodos;
public class CientificosBdd {

	public static void main(String[] args) {
		metodos.conectar();
		metodos.crearBaseDatos();
		metodos.crearTablaCientificos();
		metodos.crearTablaProyectos();
		metodos.crearTablaAsignado_A();
		metodos.insertarTablaCientificos();
		metodos.insertarTablaProyectos();
		metodos.insertarTablaAsignado_A();
		metodos.desconectar();


	}

}
