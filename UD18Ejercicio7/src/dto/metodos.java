package dto;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
public class metodos {
	public static Connection conexion; // Establecemos la constante conexion
	{
	
		
		
	
	
}
	public static void conectar() {
		try { 
			Class.forName("com.mysql.cj.jdbc.Driver");
			 conexion = DriverManager.getConnection("jdbc:mysql://192.168.1.14:3306?useTimezone=true&serverTimezone=UTC","alberto","diplodocus1");
			System.out.println("Servidor conectado"); // Hacemos la conexion
		}catch(SQLException | ClassNotFoundException ex) {
			System.out.println("No se ha podido conectar con mi base de datos");
			System.out.println(ex);
		}
	}
	public static void desconectar() {
		try {
			
			conexion.close(); // Hacemos la desconexion
			System.out.print("Desconectado con exito");
			
		} catch (SQLException ex) {
			System.out.println(ex.getMessage());
			System.out.print("Ha habido un error");
		
		}
	}
	public static void crearBaseDatos() {
		String nombre = "Cientificos"; // Creamos la base de datos
		try {
			String Query="CREATE DATABASE "+ nombre;
			Statement st= conexion.createStatement();
			st.executeUpdate(Query);
			System.out.println("DB creada con exito!");
			
			System.out.println("Se ha creado la base de datos");
		}catch(SQLException ex) {
			System.out.println(ex.getMessage());
			System.out.println("Error creando la base de datos.");
		}
	}
	
	
	public static void crearTablaCientificos() {
		String nombre = "Cientificos"; // Creamos la tabla piezas
		String nombreTabla = "Cientificos";
		try {
			String Querydb = "USE "+nombre+";";
			Statement stdb= conexion.createStatement();
			stdb.executeUpdate(Querydb);
			
			String Query = "CREATE TABLE "+nombreTabla+""
					+ "( `DNI` varchar(8) NOT NULL,\r\n" + 
					"  `NomApels` varchar(255) CHARACTER SET utf8 DEFAULT NULL,\r\n" + 
					"  PRIMARY KEY (`DNI`))";
			Statement st= conexion.createStatement();
			st.executeUpdate(Query);
			System.out.println("Tabla creada");
			
		}catch (SQLException ex){
			System.out.println(ex.getMessage());
			System.out.println("Error creando tabla.");
			
		}
		
	}
	
	public static void crearTablaAsignado_A() {
		String nombre = "Cientificos"; // Creamos la tabla suministra
		String nombreTabla = "Asignado_A";
		try {
			String Querydb = "USE "+nombre+";";
			Statement stdb= conexion.createStatement();
			stdb.executeUpdate(Querydb);
			
			String Query = "CREATE TABLE "+nombreTabla+""
					+ "  ( `Cientifico` varchar(8) NOT NULL,\r\n" + 
					"  `Proyecto` char(4) NOT NULL,\r\n" + 
					"  PRIMARY KEY (`Cientifico`,`Proyecto`),\r\n" + 
					"  FOREIGN KEY (`Cientifico`) REFERENCES `Cientificos`(`DNI`) ON DELETE CASCADE ON UPDATE CASCADE,\r\n" + 
					"  FOREIGN KEY (`Proyecto`) REFERENCES `Proyectos`(`Id`) ON DELETE CASCADE ON UPDATE CASCADE);";
			Statement st= conexion.createStatement();
			st.executeUpdate(Query);
			System.out.println("Tabla creada");
			
		}catch (SQLException ex){
			System.out.println(ex.getMessage());
			System.out.println("Error creando tabla.");
			
		}
	}
	
	public static void crearTablaProyectos() {
		String nombre = "Cientificos"; // Creamos la tabla provedoores
		String nombreTabla = "Proyectos";
		try {
			String Querydb = "USE "+nombre+";";
			Statement stdb= conexion.createStatement();
			stdb.executeUpdate(Querydb);
			
			String Query = "CREATE TABLE "+nombreTabla+""
					+ " (  `Id` char(4) NOT NULL,\r\n" + 
					"  `Nombre` varchar(255) CHARACTER SET utf8 DEFAULT NULL,\r\n" + 
					"  `Horas` int(11) DEFAULT NULL,\r\n" + 
					"  PRIMARY KEY (`Id`));";
			Statement st= conexion.createStatement();
			st.executeUpdate(Query);
			System.out.println("Tabla creada");
			
		}catch (SQLException ex){
			System.out.println(ex.getMessage());
			System.out.println("Error creando tabla.");
			
		}
	}
	
	
	
	



public static void insertarTablaCientificos() {
	String nombre = "Cientificos"; // Insertamos los datos en la tabla fabricante
	String nombreTabla = "Cientificos";
	try {
		String Querydb = "USE "+nombre+";";
		Statement stdb= conexion.createStatement();
		stdb.executeUpdate(Querydb);
					
		String Query = "INSERT INTO " + nombreTabla + " (DNI,NomApels) VALUE"
				+ " ('3990784C','Manuel'),"
				+ " ('3499784Q','Paco'),"
				+ " ('3398784A','Jose'),"
				+ " ('3296784S','Juan'),"
				+ " ('3194784F','Alberto')";
		Statement st = conexion.createStatement();
		st.executeUpdate(Query);
		
		System.out.println("Datos almacenados correctamente");;
		
	} catch (SQLException ex ) {
		System.out.println(ex.getMessage());
	System.out.println("Error en la introduccion de datos");
	}
}
public static void insertarTablaAsignado_A() {
	String nombre = "Cientificos"; // Insertamos los datos en la tabla articulos
	String nombreTabla = "Asignado_A";
	try {
		String Querydb = "USE "+nombre+";";
		Statement stdb= conexion.createStatement();
		stdb.executeUpdate(Querydb);
					
		String Query = "INSERT INTO " + nombreTabla + " (Cientifico,Proyecto) VALUE"
				+ "('3990784C','ED01'),"
				+ "('3499784Q','GE01'),"
				+ "('3398784A','OL01'),"
				+ "('3296784S','TV01'),"
				+ "('3194784F','VE01');";
		Statement st = conexion.createStatement();
		st.executeUpdate(Query);
		
		System.out.println("Datos almacenados correctamente");;
		
	} catch (SQLException ex ) {
		System.out.println(ex.getMessage());
	System.out.println("Error en la introduccion de datos");
	}
}
public static void insertarTablaProyectos() {
	String nombre = "Cientificos"; // Insertamos los datos en la tabla articulos
	String nombreTabla = "Proyectos";
	try {
		String Querydb = "USE "+nombre+";";
		Statement stdb= conexion.createStatement();
		stdb.executeUpdate(Querydb);
					
		String Query = "INSERT INTO " + nombreTabla + " (Id,Nombre,Horas) VALUE"
				+ "('ED01','Eden',340),"
				+ "('GE01','Genesis',30),"
				+ "('OL01','OLGAD',110),"
				+ "('TV01','TVE',270),"
				+ "('VE01','VENT',700);";
		Statement st = conexion.createStatement();
		st.executeUpdate(Query);
		
		System.out.println("Datos almacenados correctamente");;
		
	} catch (SQLException ex ) {
		System.out.println(ex.getMessage());
	System.out.println("Error en la introduccion de datos");
	}
}



}
