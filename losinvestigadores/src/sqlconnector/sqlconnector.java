package sqlconnector;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class sqlconnector {

	// Creo la constante conexion
	public static Connection conexion;
	
	// Creo el metodo que conectar con la base de datos
	public void startConnection() {
		
		try {
			
			Class.forName("com.mysql.cj.jdbc.Driver");
			
			conexion = DriverManager.getConnection("jdbc:mysql://192.168.1.XXX:3306?useTimezone=true&serverTimezone=UTC", "USER", "PASS");
			
			System.out.println("Server Connected");
			
		} catch (SQLException | ClassNotFoundException ex) {
			System.out.println("No se ha podido conectar con la base de datos");
			System.out.println(ex);
		}
	}
	
	// Mtodo que desconectar
	public void closeConnection() {
		
		try {
			
			conexion.close();
			System.out.println("Server Disconnected");
			
		} catch (SQLException ex) {
			System.out.println(ex.getMessage());
			System.out.println("Error cerrando conexion");
		}
	}
	
	// Mtodo que crear la base de datos
	public void createDB(String name) {
		
		try {
			String Query = "CREATE DATABASE " + name;
			Statement st = conexion.createStatement();
			st.executeUpdate(Query);
			System.out.println("DB creada con exito.");
			
			System.out.println("Se ha creado la DB " + name + " de forma exitosa.");
			
		} catch (SQLException ex) {
			System.out.println(ex.getMessage());
			System.out.println("Error creando la DB.");
		}
	}
	
	// Mtodos que crearn las tablas
	public void createTableFacultad(String db, String name) {
		
		try {
			String Querydb = "USE " + db + ";";
			
			Statement stdb = conexion.createStatement();
			
			stdb.executeUpdate(Querydb);
			
			String Query = "CREATE TABLE " + name + "(`Codigo` int NOT NULL,\r\n" + 
					"  `Nombre` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,\r\n" + 
					"  PRIMARY KEY (`Codigo`))";
				
				Statement st = conexion.createStatement();
				
				st.executeUpdate(Query);
				System.out.println("Tabla creada con exito.");
			
		} catch (SQLException ex) {
			System.out.println(ex.getMessage());
			System.out.println("Error creando tabla.");
		}
	}
	
	public void createTableInvestigadores(String db, String name) {
		
		try {
			
			String Querydb = "USE " + db + ";";
			
			Statement stdb = conexion.createStatement();
			
			stdb.executeUpdate(Querydb);
			
			String Query = "CREATE TABLE " + name + "(`DNI` varchar(8) NOT NULL,\r\n" + 
					"  `NombreApels` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,\r\n" + 
					"  `Facultad` int NOT NULL,\r\n" + 
					"  PRIMARY KEY (`DNI`))";
			
			Statement st = conexion.createStatement();
			
			st.executeLargeUpdate(Query);
			System.out.println("Tabla creada con exito");
			
		} catch (SQLException ex) {
			System.out.println(ex.getMessage());
			System.out.println("Error creando tabla.");
		}
		
	}
	
	public void createTableEquipos(String db, String name) {
		
		try {
			
			String Querydb = "USE " + db + ";";
			
			Statement stdb = conexion.createStatement();
			
			stdb.executeUpdate(Querydb);
			
			String Query = "CREATE TABLE " + name + "(`NumSerie` char(4) NOT NULL,\r\n" + 
					"  `Nombre` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,\r\n" + 
					"  `Facultad` int NOT NULL,\r\n" + 
					"  PRIMARY KEY (`NumSerie`))";
			
			Statement st = conexion.createStatement();
			
			st.executeLargeUpdate(Query);
			System.out.println("Tabla creada con exito");
			
		} catch (SQLException ex) {
			System.out.println(ex.getMessage());
			System.out.println("Error creando tabla.");
		}
		
	}

	public void createTableReserva(String db, String name) {
	
	try {
		
		String Querydb = "USE " + db + ";";
		
		Statement stdb = conexion.createStatement();
		
		stdb.executeUpdate(Querydb);
		
		String Query = "CREATE TABLE " + name + "(`DNI` varchar(8) NOT NULL,\r\n" + 
				"  `NumSerie` char(4) NOT NULL,\r\n" + 
				"  `Comienzo` datetime DEFAULT NULL,\r\n" + 
				"  `Fin` datetime DEFAULT NULL,\r\n" + 
				"  PRIMARY KEY (`DNI`,`NumSerie`))";
		
		Statement st = conexion.createStatement();
		
		st.executeLargeUpdate(Query);
		System.out.println("Tabla creada con exito");
		
	} catch (SQLException ex) {
		System.out.println(ex.getMessage());
		System.out.println("Error creando tabla.");
	}
	
}
	
	// Mtodos que insertarn los datos
	public void insertDataFacultad(String db, String tableName) {
		
		try {
			String Querydb = "USE " + db + ";";
			
			Statement stdb = conexion.createStatement();
			
			stdb.executeLargeUpdate(Querydb);
			
			String Query = "INSERT INTO " + tableName + " (Codigo, Nombre) VALUE"
																			+ "(1, 'URV'),"
																			+ "(2, 'UCAM'),"
																			+ "(3, 'UOC'),"
																			+ "(4, 'UPC'),"
																			+ "(5, 'UAB');";
			
			Statement st = conexion.createStatement();
			st.executeUpdate(Query);
			
			System.out.println("Datos almacenados correctamente");
			
		} catch (SQLException ex) {
			System.out.println(ex.getMessage());
			System.out.println("Error en el almacenamiento");
		}
	}
	
	public void insertDataInvestigadores(String db, String tableName) {
		
		try {
			String Querydb = "USE " + db + ";";
			
			Statement stdb = conexion.createStatement();
			
			stdb.executeLargeUpdate(Querydb);
			
			String Query = "INSERT INTO " + tableName + " (DNI, NombreApels, Facultad) VALUE"
																						+ "('1234567H', 'Aitor Alarcn', '1'),"
																						+ "('1234567J', 'Jess Cristo', '2'),"
																						+ "('1234567M', 'Alberto Garca', '3'),"
																						+ "('1234567N', 'Oscar Trillas', '4'),"
																						+ "('1234567R', 'Aleix Jimenez', '5');";
			
			Statement st = conexion.createStatement();
			st.executeUpdate(Query);
			
			System.out.println("Datos almacenados correctamente");
			
		} catch (SQLException ex) {
			System.out.println(ex.getMessage());
			System.out.println("Error en el almacenamiento");
		}
		
	}
	
	public void insertDataEquipo(String db, String tableName) {
		
		try {
			String Querydb = "USE " + db + ";";
			
			Statement stdb = conexion.createStatement();
			
			stdb.executeLargeUpdate(Querydb);
			
			String Query = "INSERT INTO " + tableName + " (NumSerie, Nombre, Facultad) VALUE"
																						+ "('GFD5', 'Stronso', '1'),"
																						+ "('SGS6', 'Bambino', '2'),"
																						+ "('AAS7', 'Forno', '3'),"
																						+ "('VVG5', 'Pietra', '4'),"
																						+ "('AGT9', 'DiMama', '5');";
			
			Statement st = conexion.createStatement();
			st.executeUpdate(Query);
			
			System.out.println("Datos almacenados correctamente");
			
		} catch (SQLException ex) {
			System.out.println(ex.getMessage());
			System.out.println("Error en el almacenamiento");
		}
		
	}

	public void insertDataReserva(String db, String tableName) {
	
	try {
		String Querydb = "USE " + db + ";";
		
		Statement stdb = conexion.createStatement();
		
		stdb.executeLargeUpdate(Querydb);
		
		String Query = "INSERT INTO " + tableName + " (DNI, NumSerie, Comienzo, Fin) VALUE"
																					+ "('1234567Q', 'GFD5', '23-12-12', '05-02-13'),"
																					+ "('1234567W', 'SGS6', '05-10-15', '20-10-20'),"
																					+ "('1234567E', 'AAS7', '01-01-10', '05-08-19'),"
																					+ "('1234567R', 'VVG5', '05-06-12', '03-06-19'),"
																					+ "('1234567T', 'AGT9', '30-06-01', '30-06-20');";
		
		Statement st = conexion.createStatement();
		st.executeUpdate(Query);
		
		System.out.println("Datos almacenados correctamente");
		
	} catch (SQLException ex) {
		System.out.println(ex.getMessage());
		System.out.println("Error en el almacenamiento");
	}
	
}

	

}
