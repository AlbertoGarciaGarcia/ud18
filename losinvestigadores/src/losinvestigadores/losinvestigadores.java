package losinvestigadores;

import sqlconnector.sqlconnector;

public class losinvestigadores {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		// Creo el objeto conexion
		sqlconnector conexion = new sqlconnector();
		
		// Me conecto a la base de datos
		conexion.startConnection();
		
		// Creo la base de datos
		conexion.createDB("losinvestigadores");
		
		// Creo las tablas
		conexion.createTableFacultad("losinvestigadores", "facultad");
		conexion.createTableInvestigadores("losinvestigadores", "investigadores");
		conexion.createTableEquipos("losinvestigadores", "equipos");
		conexion.createTableReserva("losinvestigadores", "reserva");
		
		// Inserto datos
		conexion.insertDataFacultad("losinvestigadores", "facultad");
		conexion.insertDataInvestigadores("losinvestigadores", "investigadores");
		conexion.insertDataEquipo("losinvestigadores", "equipos");
		conexion.insertDataReserva("losinvestigadores", "reserva");
		
		// Cierro la conexion
		conexion.closeConnection();
		
	}

}
