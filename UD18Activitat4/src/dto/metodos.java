package dto;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class metodos {
	
	public static Connection conexion;
	//Metodo de conexion
	public static void conexion() {
		try {
	        Class.forName("com.mysql.cj.jdbc.Driver");
	        conexion = DriverManager.getConnection("jdbc:mysql://192.168.1.44:3306?useTimezone=true&serverTimezone=UTC","remot","Diplodocus1.");
	        System.out.println("Servidor conectado");
	    }catch(SQLException | ClassNotFoundException ex) {
	        System.out.println("No se ha conectado");
	        System.out.println(ex);
	    }
		
	}
	
	//Metodo para cerrar la conexion
	public static void cerrarConexion() {
		try {
			conexion.close();
			System.out.print("Server Disconnected");
		} catch (SQLException ex) {
			System.out.println(ex.getMessage());
			System.out.print("Error cerrando conexion");
		}
	}
	
	//Metodo para crear una tabla
	public static void crearTabla(String db, String nombre, String insertar) {
		try {
			String querydb = "USE  " + db + ";";
			Statement stdb = conexion.createStatement();
			stdb.executeLargeUpdate(querydb);
			String query = "CREATE TABLE " + nombre + "" + insertar;
			Statement st = conexion.createStatement();
			st.executeLargeUpdate(query);
			System.out.println("Tabla Creada");
			
		}catch (SQLException ex){
			System.out.println(ex.getMessage());
			System.out.println("Error crando tabla.");
			
		}
	}
	
	//Metodo para crear una base de datos
	public static void crearBaseDatos(String nombre) {
		try {
			String Query="CREATE DATABASE "+ nombre;
			Statement st= conexion.createStatement();
			st.executeUpdate(Query);
			System.out.println("DB creada con exito!");	
			System.out.println("Base de datos creada");
		}catch(SQLException ex) {
			System.out.println(ex.getMessage());
			System.out.println("Error creando la DB.");
		}	
	}
	
	//Metodo para insertar valores dentro de la tabla peliculas
	public static void insertarValoresPeliculas(String db, String nombreTabla, int codigo, String nombre, int calificacion) {
		try {
			String Querydb = "USE " + db + ";";
			Statement stdb = conexion.createStatement();
			stdb.executeUpdate(Querydb);
			String Query = "INSERT INTO " + nombreTabla + "(codigo, nombre, calificacion) VALUE("
							+ "\"" + codigo + "\","
							+ "\"" + nombre + "\","
							+ "\"" + calificacion + "\");";
			Statement st = conexion.createStatement();
			st.executeUpdate(Query);
			
			System.out.println("Datos guardados");
		} catch (SQLException ex) {
			System.out.println("No se ha podido guardar los datos");
			System.out.println(ex.getMessage());
		}
	}
	
	//Metodo para insertar valores dentro de la tabla Salas
	public static void insertarValoresSalas(String db, String nombreTabla, int codigo, String nombre, int pelicula) {
		try {
			String Querydb = "USE " + db + ";";
			Statement stdb = conexion.createStatement();
			stdb.executeUpdate(Querydb);
			String Query = "INSERT INTO " + nombreTabla + "(codigo, nombre, pelicula) VALUE("
							+ "\"" + codigo + "\","
							+ "\"" + nombre + "\","
							+ "\"" + pelicula + "\");";
			Statement st = conexion.createStatement();
			st.executeUpdate(Query);
			
			System.out.println("Datos guardados");
		} catch (SQLException ex) {
			System.out.println("No se ha podido guardar los datos");
			System.out.println(ex.getMessage());
		}
	}
	
	//Metodo que inserta todos los valores que quiero en las dos tablas
	public static void introducirInformacion() {
		//Inserto valores en la primera tabla
		insertarValoresPeliculas("PeliculasISalas", "peliculas", 1, "Bugs Bunny", 8);
		insertarValoresPeliculas("PeliculasISalas", "peliculas", 2, "Avengers", 12);
		insertarValoresPeliculas("PeliculasISalas", "peliculas", 3, "One piece", 14);
		insertarValoresPeliculas("PeliculasISalas", "peliculas", 4, "Ready player one", 10);
		insertarValoresPeliculas("PeliculasISalas", "peliculas", 5, "Minecraft", 6);
		
		//Inserto valores en la segunda tabla
		insertarValoresPeliculas("PeliculasISalas", "salas", 1, "Sala 1", 1);
		insertarValoresPeliculas("PeliculasISalas", "salas", 2, "Sala 2", 2);
		insertarValoresPeliculas("PeliculasISalas", "salas", 3, "Sala 3", 3);
		insertarValoresPeliculas("PeliculasISalas", "salas", 4, "Sala 4", 4);
		insertarValoresPeliculas("PeliculasISalas", "salas", 5, "Sala 5", 5);
	}
}
