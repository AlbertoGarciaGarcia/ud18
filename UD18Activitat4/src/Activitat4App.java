import dto.metodos;

public class Activitat4App {

	public static void main(String[] args) {
		metodos.conexion();
		metodos.crearBaseDatos("PeliculasISalas");
		metodos.crearTabla("PeliculasISalas", "peliculas", "(codigo int PRIMARY KEY, nombre nvarchar(100), calificacion int)");
		metodos.crearTabla("PeliculasISalas", "salas", "(codigo int, nombre nvarchar(100), pelicula int, PRIMARY KEY (codigo), FOREIGN KEY(pelicula) REFERENCES peliculas(codigo))");
		metodos.introducirInformacion();
		metodos.cerrarConexion();

	}

}
