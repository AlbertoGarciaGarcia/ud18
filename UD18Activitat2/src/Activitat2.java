import dto.metodos;

public class Activitat2 {

	public static void main(String[] args) {
		metodos.conexion();
		metodos.crearBaseDatos("EMPLEADOS");
		metodos.crearTabla("EMPLEADOS", "departamentos", "(codigo int PRIMARY KEY, nombre nvarchar(100), presupuesto int)");
		metodos.crearTabla("EMPLEADOS", "empleados", "(DNI varchar(8), nombre nvarchar(100), apellidos nvarchar(255), departamento int, PRIMARY KEY (DNI), FOREIGN KEY(departamento) REFERENCES departamentos(codigo))");
		metodos.introducirInformacion();
		metodos.cerrarConexion();
	}

}
