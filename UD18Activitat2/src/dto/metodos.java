package dto;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;


public class metodos {
	
	public static Connection conexion;
	//Metodo de conexion
	public static void conexion() {
		try {
	        Class.forName("com.mysql.cj.jdbc.Driver");
	        conexion = DriverManager.getConnection("jdbc:mysql://192.168.1.44:3306?useTimezone=true&serverTimezone=UTC","remot","Diplodocus1.");
	        System.out.println("Servidor conectado");
	    }catch(SQLException | ClassNotFoundException ex) {
	        System.out.println("No se ha conectado");
	        System.out.println(ex);
	    }
		
	}
	
	//Metodo para cerrar la conexion
	public static void cerrarConexion() {
		try {
			conexion.close();
			System.out.print("Server Disconnected");
		} catch (SQLException ex) {
			System.out.println(ex.getMessage());
			System.out.print("Error cerrando conexion");
		}
	}
	
	//Metodo para crear una tabla
	public static void crearTabla(String db, String nombre, String insertar) {
		try {
			String querydb = "USE  " + db + ";";
			Statement stdb = conexion.createStatement();
			stdb.executeLargeUpdate(querydb);
			String query = "CREATE TABLE " + nombre + "" + insertar;
			Statement st = conexion.createStatement();
			st.executeLargeUpdate(query);
			System.out.println("Tabla Creada");
			
		}catch (SQLException ex){
			System.out.println(ex.getMessage());
			System.out.println("Error crando tabla.");
			
		}
	}
	
	//Metodo para crear una base de datos
	public static void crearBaseDatos(String nombre) {
		try {
			String Query="CREATE DATABASE "+ nombre;
			Statement st= conexion.createStatement();
			st.executeUpdate(Query);
			System.out.println("DB creada con exito!");	
			System.out.println("Base de datos creada");
		}catch(SQLException ex) {
			System.out.println(ex.getMessage());
			System.out.println("Error creando la DB.");
		}	
	}
	
	//Metodo para insertar valores dentro de la tabla departamento
	public static void insertarValoresDepartamentos(String db, String nombreTabla, int codigo, String nombre, int presupuesto) {
		try {
			String Querydb = "USE " + db + ";";
			Statement stdb = conexion.createStatement();
			stdb.executeUpdate(Querydb);
			String Query = "INSERT INTO " + nombreTabla + "(codigo, nombre, presupuesto) VALUE("
							+ "\"" + codigo + "\","
							+ "\"" + nombre + "\","
							+ "\"" + presupuesto + "\");";
			Statement st = conexion.createStatement();
			st.executeUpdate(Query);
			
			System.out.println("Datos guardados");
		} catch (SQLException ex) {
			System.out.println("No se ha podido guardar los datos");
			System.out.println(ex.getMessage());
		}
	}
	
	//Metodo para insertar valores dentro de la tabla empleados
	public static void insertarValoresEmpleados(String db, String nombreTabla, int DNI, String nombre, String apellidos, int departamento) {
		try {
			String Querydb = "USE " + db + ";";
			Statement stdb = conexion.createStatement();
			stdb.executeUpdate(Querydb);
			String Query = "INSERT INTO " + nombreTabla + "(DNI, nombre, apellidos, departamento) VALUE("
							+ "\"" + DNI + "\","
							+ "\"" + nombre + "\","
							+ "\"" + apellidos + "\","
							+ "\"" + departamento + "\");";
			Statement st = conexion.createStatement();
			st.executeUpdate(Query);
			
			System.out.println("Datos guardados");
		} catch (SQLException ex) {
			System.out.println("No se ha podido guardar los datos");
			System.out.println(ex.getMessage());
		}
	}
	
	//Metodo que inserta todos los valores que quiero en las dos tablas
	public static void introducirInformacion() {
		//Inserto valores en la primera tabla
		insertarValoresDepartamentos("EMPLEADOS", "departamentos", 1, "Compras", 10000);
		insertarValoresDepartamentos("EMPLEADOS", "departamentos", 2, "Vendas", 12000);
		insertarValoresDepartamentos("EMPLEADOS", "departamentos", 3, "Online", 5000);
		insertarValoresDepartamentos("EMPLEADOS", "departamentos", 4, "Tecnico", 20000);
		insertarValoresDepartamentos("EMPLEADOS", "departamentos", 5, "Limpieza", 2000);
		
		//Inserto valores en la segunda tabla
		insertarValoresEmpleados("EMPLEADOS", "empleados", 12345678, "Oscar", "Trillas", 1);
		insertarValoresEmpleados("EMPLEADOS", "empleados", 87654321, "Ana", "Fuentes", 2);
		insertarValoresEmpleados("EMPLEADOS", "empleados", 13245365, "Dani", "Garcia", 3);
		insertarValoresEmpleados("EMPLEADOS", "empleados", 54361784, "Aitor", "Calv�", 3);
		insertarValoresEmpleados("EMPLEADOS", "empleados", 75830135, "Sergi", "Adell", 5);
	}
}
