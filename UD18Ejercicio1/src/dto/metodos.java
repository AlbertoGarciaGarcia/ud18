package dto;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
public class metodos {
	public static Connection conexion; // Establecemos la constante conexion
	{
	
		
		
	
	
}
	public static void conectar() {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			 conexion = DriverManager.getConnection("jdbc:mysql://192.168.1.14:3306?useTimezone=true&serverTimezone=UTC","alberto","diplodocus1");
			System.out.println("Servidor conectado"); // Hacemos la conexion
		}catch(SQLException | ClassNotFoundException ex) {
			System.out.println("No se ha podido conectar con mi base de datos");
			System.out.println(ex);
		}
	}
	public static void desconectar() {
		try {
			
			conexion.close(); // Hacemos la desconexion
			System.out.print("Desconectado con exito");
			
		} catch (SQLException ex) {
			System.out.println(ex.getMessage());
			System.out.print("Ha habido un error");
		
		}
	}
	public static void crearBaseDatos() {
		String nombre = "Tienda"; // Creamos la base de datos
		try {
			String Query="CREATE DATABASE "+ nombre;
			Statement st= conexion.createStatement();
			st.executeUpdate(Query);
			System.out.println("DB creada con exito!");
			
			System.out.println("Se ha creado la base de datos");
		}catch(SQLException ex) {
			System.out.println(ex.getMessage());
			System.out.println("Error creando la base de datos.");
		}
	}
	
	
	public static void crearTablaFabricantes() {
		String nombre = "Tienda"; // Creamos la tabla fabricantes
		String nombreTabla = "Fabricantes";
		try {
			String Querydb = "USE "+nombre+";";
			Statement stdb= conexion.createStatement();
			stdb.executeUpdate(Querydb);
			
			String Query = "CREATE TABLE "+nombreTabla+""
					+ "( `Codigo` int(11) NOT NULL AUTO_INCREMENT," + 
					"  `Nombre` varchar(100) CHARACTER SET utf8 DEFAULT NULL," + 
					"  PRIMARY KEY (`Codigo`))";
			Statement st= conexion.createStatement();
			st.executeUpdate(Query);
			System.out.println("Tabla creada");
			
		}catch (SQLException ex){
			System.out.println(ex.getMessage());
			System.out.println("Error creando tabla.");
			
		}
		
	}
	
	public static void crearTablaArticulos() {
		String nombre = "Tienda"; // Creamos la tabla articulos
		String nombreTabla = "Articulos";
		try {
			String Querydb = "USE "+nombre+";";
			Statement stdb= conexion.createStatement();
			stdb.executeUpdate(Querydb);
			
			String Query = "CREATE TABLE "+nombreTabla+""
					+ " (`Codigo` int(11) NOT NULL AUTO_INCREMENT,\r\n" + 
					"  `Nombre` varchar(100) CHARACTER SET utf8 DEFAULT NULL,\r\n" + 
					"  `Precio` int(11) DEFAULT NULL,\r\n" + 
					"  `Fabricante` int(11) DEFAULT NULL,\r\n" + 
					"  PRIMARY KEY (`Codigo`),\r\n" + 
					"  KEY `Fabricante` (`Fabricante`),\r\n" + 
					"  CONSTRAINT `ARTICULOS` FOREIGN KEY (`Fabricante`) REFERENCES `Fabricantes` (`Codigo`) ON "
					+ "DELETE CASCADE ON UPDATE CASCADE);";
			Statement st= conexion.createStatement();
			st.executeUpdate(Query);
			System.out.println("Tabla creada");
			
		}catch (SQLException ex){
			System.out.println(ex.getMessage());
			System.out.println("Error creando tabla.");
			
		}
	}
	
	
	
	



public static void insertarTablaFabricantes() {
	String nombre = "Tienda"; // Insertamos los datos en la tabla fabricante
	String nombreTabla = "Fabricantes";
	try {
		String Querydb = "USE "+nombre+";";
		Statement stdb= conexion.createStatement();
		stdb.executeUpdate(Querydb);
					
		String Query = "INSERT INTO " + nombreTabla + " (Codigo,Nombre) VALUE"
				+ " (1,'Asus'),"
				+ "(2,'Aog'),"
				+ "(3,'BTS'),"
				+ "(4,'AAD'),"
				+ "(5,'OTF');";
		Statement st = conexion.createStatement();
		st.executeUpdate(Query);
		
		System.out.println("Datos almacenados correctamente");;
		
	} catch (SQLException ex ) {
		System.out.println(ex.getMessage());
	System.out.println("Error en la introduccion de datos");
	}
}
public static void insertarTablaArticulos() {
	String nombre = "Tienda"; // Insertamos los datos en la tabla articulos
	String nombreTabla = "Articulos";
	try {
		String Querydb = "USE "+nombre+";";
		Statement stdb= conexion.createStatement();
		stdb.executeUpdate(Querydb);
					
		String Query = "INSERT INTO " + nombreTabla + " (Codigo,Nombre,Precio,Fabricante) VALUE"
				+ "(1,'Caja',40,1),"
				+ "(2,'Tele',10,2),"
				+ "(3,'Silla',120,3),"
				+ "(4,'Cajones',125,4),"
				+ "(5,'Nevera',100,5);";
		Statement st = conexion.createStatement();
		st.executeUpdate(Query);
		
		System.out.println("Datos almacenados correctamente");;
		
	} catch (SQLException ex ) {
		System.out.println(ex.getMessage());
	System.out.println("Error en la introduccion de datos");
	}
}



}
